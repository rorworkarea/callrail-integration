require 'workarea/testing/teaspoon'

Teaspoon.configure do |config|
  config.root = Workarea::Callrail::Engine.root
  Workarea::Teaspoon.apply(config)
end
