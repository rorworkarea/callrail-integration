require 'test_helper'

module Workarea
  class WebWidgetSystemTest < Workarea::SystemTest
    def test_callrail_script
      Workarea.with_config do |config|
        config.callrail_script_src = '//cdn.callrail.com/companies/12/swap.js'
        visit storefront.root_path

        assert page.has_xpath?('//script[@id="cr-snippet"]', visible: false)

        config.callrail_script_src = nil
        visit storefront.root_path
        
        assert page.has_no_xpath?('//script[@id="cr-snippet"]', visible: false)
      end
    end

    def test_tracking_num
      Workarea.with_config do |config|
        config.callrail_target_num = '8205823687'
        visit storefront.root_path

        assert(page.has_content?('8205823687'))
      end
    end
  end
end