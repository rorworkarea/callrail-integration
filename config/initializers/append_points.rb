Workarea::Plugin.append_partials(
  'storefront.body_top', 
  'workarea/storefront/callrail_script_snippet'
)

Workarea::Plugin.append_partials(
  'storefront.footer_help', 
  'workarea/storefront/tracking_num_footer'
)

Workarea::Plugin.append_partials(
  'storefront.page-header__callrails', 
  'workarea/storefront/tracking_num'
)

Workarea::Plugin.append_stylesheets(
  'storefront.components',
  'workarea/storefront/components/tracking_num'
)