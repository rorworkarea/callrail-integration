Workarea.configure do |config|
  config.callrail_target_num = Rails.application.secrets.callrail_target_num
  config.callrail_script_src = Rails.application.secrets.callrail_script_src
end
