$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "workarea/callrail/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "workarea-callrail"
  spec.version     = Workarea::Callrail::VERSION
  spec.authors     = ["Laxmi Pattar"]
  spec.email       = ["laxmi.p@trikatechnologies.com"]
  spec.homepage    = "http://workarea.com"
  spec.summary     = "Workarea Callrail."
  spec.description = "Tracking and analytics for phone calls and web forms. Optimize your marketing and increase ROI on your PPC, SEO, and offline ad campaigns."
  spec.license     = "Business Software License"


  spec.files = `git ls-files`.split("\n")

  spec.add_dependency 'workarea', '~> 3.x'
end
