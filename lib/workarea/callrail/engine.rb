require 'workarea/callrail'

module Workarea
  module Callrail
    class Engine < ::Rails::Engine
      include Workarea::Plugin
      isolate_namespace Workarea::Callrail
    end
  end
end
