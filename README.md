Workarea Callrail
================================================================================

A Workarea Commerce plugin for Callrail.

Overview
--------------------------------------------------------------------------------

Tracking and analytics for phone calls and web forms. Optimize your marketing and increase ROI on your PPC, SEO, and offline ad campaigns.

Getting Started
--------------------------------------------------------------------------------

This gem contains a Rails engine that must be mounted onto a host Rails application.

Then add the gem to your application's Gemfile specifying the source:

    # ...
    gem 'workarea-callrail'
    # ...

Update your application's bundle.

    cd path/to/application
    bundle

Configuration.

To integrate your Workarea application with CallRail you need to configure your host application with your CallRail script source and swap target number on the configuration page.

		#...
		callrail_target_num = ""

		callrail_script_src = ""
		#...

Features
--------------------------------------------------------------------------------

### TODO

Workarea Platform Documentation
--------------------------------------------------------------------------------

See [https://developer.workarea.com](https://developer.workarea.com) for Workarea platform documentation.

License
--------------------------------------------------------------------------------

Workarea Callrail is released under the [Business Software License](LICENSE)
